package com.example.pre_examen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnIMC;
    private Button btnConvertidor;
    private Button btnSalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtNombre =(EditText)findViewById(R.id.txtNombre);
        btnIMC = (Button)findViewById(R.id.btnIMC);
        btnConvertidor = (Button)findViewById(R.id.btnConvertidor);
        btnSalir = (Button)findViewById(R.id.btnSalir);

        btnIMC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this,"Falto capturar el nombre: ", Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent(MainActivity.this,IMCActivity.class);
                    intent.putExtra("nombre",nombre);


                    startActivity(intent);

                }
            }
        });
        btnConvertidor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtNombre.getText().toString();
                if(nombre.matches("")){
                    Toast.makeText(MainActivity.this,"Falto capturar el nombre: ", Toast.LENGTH_SHORT).show();
                }
                else{
                    Intent intent = new Intent(MainActivity.this,ConvertidorActivity.class);
                    intent.putExtra("nombre",nombre);


                    startActivity(intent);

                }
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
            }

    }

