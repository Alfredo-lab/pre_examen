package com.example.pre_examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ConvertidorActivity extends AppCompatActivity {
    private TextView lblNom;
    private EditText txtCan;
    private Button btnCal;
    private Button btnLim;
    private Button btnCerrar;
    private RadioGroup rGroup;
    private RadioButton Cel;
    private RadioButton Fah;
    private TextView lblResult;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_convertidor);
        lblNom=(TextView)findViewById(R.id.lblNom);
        Bundle datos = getIntent().getExtras();
        lblNom.setText("Bienvenido: "+ datos.getString("nombre"));
        txtCan= (EditText) findViewById(R.id.txtCan);
        btnCal=(Button) findViewById(R.id.btnCal);
        btnLim=(Button) findViewById(R.id.btnLim);
        btnCerrar= (Button) findViewById(R.id.btnCerrar);
        lblNom=(TextView)findViewById(R.id.lblNom);
        rGroup=(RadioGroup)findViewById(R.id.rGroup);
        Cel=(RadioButton)findViewById(R.id.Cel);
        Fah=(RadioButton)findViewById(R.id.Fah);
        lblResult=(TextView)findViewById(R.id.lblResult);

        btnCal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String val = txtCan.getText().toString();
                if(val.matches("")){
                    Toast.makeText(ConvertidorActivity.this,"Falto capturar la cantidad: ", Toast.LENGTH_SHORT).show();
                }else{
                    float tol=0;
                    float c = Float.valueOf(txtCan.getText().toString());
                    if(Cel.isChecked()){
                        tol= (float) ((1.8*c)+32);
                    }else
                        if(Fah.isChecked()){
                            tol=(c-32)*5/9;
                        }
                        lblResult.setText("El resultado es: "+ tol);
                    }

                }


        });
        btnLim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtCan.setText("");
                lblResult.setText("El resultado es: ");
                Cel.setChecked(true);
            }
        });
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
