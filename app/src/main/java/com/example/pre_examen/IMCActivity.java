package com.example.pre_examen;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class IMCActivity extends AppCompatActivity {
    private TextView lblNombre;
    private EditText txtMetros;
    private EditText txtCentimetros;
    private EditText txtPeso;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblimc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imc2);
        lblNombre=(TextView)findViewById(R.id.lblNombre);

        Bundle datos = getIntent().getExtras();
        lblNombre.setText("Bienvenido: "+ datos.getString("nombre"));
        txtMetros =(EditText)findViewById(R.id.txtMetros);
        txtCentimetros =(EditText)findViewById(R.id.txtCentimetros);
        txtPeso =(EditText)findViewById(R.id.txtPeso);
        btnCalcular=(Button)findViewById(R.id.btnCalcular);
        btnLimpiar=(Button)findViewById(R.id.btnLimpiar);
        btnRegresar=(Button)findViewById(R.id.btnRegresar);
        lblimc=(TextView)findViewById(R.id.lblimc);

btnCalcular.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String m=txtMetros.getText().toString();
        String cm=txtCentimetros.getText().toString();
        String p=txtPeso.getText().toString();
        if(m.matches("")||cm.matches("")||p.matches("")){
            Toast.makeText(IMCActivity.this,"Falto capturar algun dato: ", Toast.LENGTH_SHORT).show();
        }else {
            float n = Float.valueOf(txtMetros.getText().toString());
            float c = Float.valueOf(txtCentimetros.getText().toString());
            float pe = Float.valueOf(txtPeso.getText().toString());
            float t=0;
            c=c/100;
            n=n+c;
            t=pe/(n*n);

            lblimc.setText("IMC: "+t);
        }
    }
});
btnLimpiar.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
    txtMetros.setText("");
    txtCentimetros.setText("");
    txtPeso.setText("");
    lblimc.setText("IMC: ");
    }
});
btnRegresar.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        finish();
    }
});
}
}